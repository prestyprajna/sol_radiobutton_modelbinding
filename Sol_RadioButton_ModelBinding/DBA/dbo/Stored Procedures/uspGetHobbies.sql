﻿CREATE PROCEDURE [dbo].[uspGetHobbies]
	@Command VARCHAR(MAX),

	--@HobbyId NUMERIC(18,0),
	--@HobbyName VARCHAR(50),
	--@Interested BIT,
	@UserId NUMERIC(18,0),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	
		IF @Command='SELECT'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT H.HobbyId,
				H.HobbyName,
				H.Interested,
				H.UserId
					FROM tblHobbies AS H
						WHERE H.UserId=@UserId

				SET @Status=1
				SET @Message='SELECT SUCCESSFULL'

					COMMIT TRANSACTION


			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='SELECT EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

		ELSE IF @Command='SELECT_ALL_DATA'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT H.HobbyId,
				H.HobbyName,
				H.Interested,
				H.UserId
					FROM tblHobbies AS H
						

				SET @Status=1
				SET @Message='SELECT ALL DATA SUCCESSFULL'

					COMMIT TRANSACTION


			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='SELECT ALL DATA EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

	END