﻿CREATE PROCEDURE uspSetUser
(
	@Command VARCHAR(MAX),
		
	@UserID numeric(18,0),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@DateOfBirth DATETIME,
	@Age INT,
	@Gender BIT,
	@CityId INT,
	@ImagePath NVARCHAR(MAX),
	@MobileNo VARCHAR(10),

	@GetUserId NUMERIC(18,0) OUT,	

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	
		IF @Command='INSERT'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				INSERT INTO tblUser
				(
					FirstName,
					LastName,
					DateOfBirth,
					Age,
					Gender,
					CityId,
					ImagePath,
					MobileNo
				)
				VALUES
				(
					@FirstName,
					@LastName,
					@DateOfBirth,
					@Age,
					@Gender,
					@CityId,
					@ImagePath,
					@MobileNo
				)

				SET @GetUserId=@@IDENTITY
				SET @Status=1
				SET @Message='INSERT SUCCESSFULL'

					COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='INSERT EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

		ELSE IF @Command='UPDATE'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY			
			
				SELECT 
				--@UserID=CASE WHEN @UserID IS NULL THEN u.UserID  ELSE @UserID END,
				@FirstName=CASE WHEN @FirstName IS NULL THEN u.FirstName  ELSE @FirstName END,
				@LastName=CASE WHEN @LastName IS NULL THEN u.LastName  ELSE @LastName END,
				@DateOfBirth=CASE WHEN @DateOfBirth IS NULL THEN u.DateOfBirth  ELSE @DateOfBirth END,
				@Age=CASE WHEN @Age IS NULL THEN u.Age  ELSE @Age END,
				@Gender=CASE WHEN @Gender IS NULL THEN u.Gender  ELSE @Gender END,
				@ImagePath=CASE WHEN @ImagePath IS NULL THEN u.ImagePath  ELSE @ImagePath END,
				@CityId=CASE WHEN @CityId IS NULL THEN u.CityId  ELSE @CityId END,
				@MobileNo=CASE WHEN @MobileNo IS NULL THEN u.MobileNo ELSE @MobileNo END
					FROM tblUser AS U
						WHERE U.UserID=@UserID

				UPDATE tblUser
					SET --UserID=@UserID,
					FirstName=@FirstName,
					LastName=@LastName,
					DateOfBirth=@DateOfBirth,
					Age=@Age,
					Gender=@Gender,
					ImagePath=@ImagePath,
					CityId=@CityId,
					MobileNo=@MobileNo
						WHERE UserID=@UserID



				SET @Status=1
				SET @Message='UPDATE SUCCESSFULL'

					COMMIT TRANSACTION


			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='UPDATE EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

	END