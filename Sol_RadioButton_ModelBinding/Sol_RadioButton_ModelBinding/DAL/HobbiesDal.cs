﻿using Sol_RadioButton_ModelBinding.DAL.ORD;
using Sol_RadioButton_ModelBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_RadioButton_ModelBinding.DAL
{
    public class HobbiesDal
    {
        #region declaration
        private HobbiesDCDataContext _dc = null;
        #endregion

        #region  constructor
        public HobbiesDal()
        {
            _dc = new HobbiesDCDataContext();
        }
        #endregion

        #region  public method

        public async Task<IEnumerable<HobbiesEntity>> GetHobbiesData()
        {
            int? status = null;
            string message = null;

            return await Task.Run(() =>
            {
                var getQuery =
                _dc?.uspGetHobbies(
                    "SELECT",
                    1,
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((GetHobbiesResultObj) => new HobbiesEntity()
                    {
                        HobbyId = GetHobbiesResultObj?.HobbyId,
                        HobbyName = GetHobbiesResultObj?.HobbyName
                    })
                    ?.ToList();

                return getQuery;
            });
        }


        #endregion
    }
}