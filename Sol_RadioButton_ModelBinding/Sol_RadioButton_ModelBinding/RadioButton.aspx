﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="RadioButton.aspx.cs" Inherits="Sol_RadioButton_ModelBinding.RadioButton" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                <asp:RadioButtonList id="rbHobbies" runat="server" DataTextField="HobbyId" DataValueField="HobbyName"  SelectMethod="BindHobbiesData"></asp:RadioButtonList>
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
