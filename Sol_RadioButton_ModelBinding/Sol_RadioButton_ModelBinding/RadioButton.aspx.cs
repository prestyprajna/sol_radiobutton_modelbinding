﻿using Sol_RadioButton_ModelBinding.DAL;
using Sol_RadioButton_ModelBinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_RadioButton_ModelBinding
{
    public partial class RadioButton : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region  public method

        public async Task<IEnumerable<HobbiesEntity>> BindHobbiesData()
        {
            return await new HobbiesDal().GetHobbiesData();
        }

        #endregion
    }
}